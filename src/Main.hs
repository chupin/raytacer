module Main where

import Codec.Picture

type Vector3 = (Float, Float, Float) 

data Shape = Sphere { sOrigin :: !Vector3, sRadius :: Float, sColor :: !Vector3 } | 
             Plane { pOrigin :: !Vector3, pNormal :: !Vector3, pColor :: !Vector3 } deriving (Show)

data Ray = Ray { rOrigin :: !Vector3, rDirection :: !Vector3 } deriving (Show)

data Hit = Hit { hColor :: !Vector3 } deriving (Show)

type World = [Shape]

dot :: Vector3 -> Vector3 -> Float
dot (x0,y0,z0) (x1,y1,z1) = ( x0*x1 + y0*y1 + z0*z1 )

sub :: Vector3 -> Vector3 -> Vector3
sub (x0,y0,z0) (x1,y1,z1) = ( x0-x1, y0-y1, z0-z1 )

add :: Vector3 -> Vector3 -> Vector3
add (x0,y0,z0) (x1,y1,z1) = ( x0+x1, y0+y1, z0+z1 )

vmul :: Vector3 -> Vector3 -> Vector3
vmul (x0,y0,z0) (x1,y1,z1) = (x0*x1, y0*y1, z0*z1)

mul :: Vector3 -> Float -> Vector3
mul (x0,y0,z0) s = (x0*s, y0*s, z0*s)

rcpMul :: Vector3 -> Float -> Vector3
rcpMul (x0,y0,z0) s = (x0/s, y0/s, z0/s)

(<.>) :: Vector3 -> Vector3 -> Float
(<.>) = dot

(<->) :: Vector3 -> Vector3 -> Vector3
(<->) = sub

(<+>) :: Vector3 -> Vector3 -> Vector3
(<+>) = add

(<**>) :: Vector3 -> Vector3 -> Vector3
(<**>) = vmul

(<*~) :: Vector3 -> Float -> Vector3
(<*~) = mul

(</~) :: Vector3 -> Float -> Vector3
(</~) = rcpMul

len :: Vector3 -> Float
len v = sqrt( v <.> v )

normalize :: Vector3 -> Vector3
normalize v = v </~ ( len v )

reflect :: Vector3 -> Vector3 -> Vector3
reflect d n = d <-> ( n <*~ ( ( d <.> n ) * 2 ) )

refract :: Vector3 -> Vector3 -> Float -> Vector3
refract d n i = if ( n <.> ( d <*~ (-1) ) > 0 ) then ( d <*~ i ) <+> ( n <*~ ( i * cos1 - cos2 ) )  else ( d <*~ i ) <+> ( n <*~ ( i * cos1 + cos2 ) ) where
    cos1 = n <.> ( d <*~ (-1) )
    cos2 = sqrt( 1 - i * i * ( 1 - cos1 * cos1 ) )

saturateV :: Vector3 -> Vector3 
saturateV (x,y,z) = (x',y',z') where
    x' = min 1.0 ( max x 0.0 )
    y' = min 1.0 ( max y 0.0 )
    z' = min 1.0 ( max z 0.0 )

saturateS :: Float -> Float
saturateS x = min 1.0 ( max x 0.0 )

mkRay :: Vector3 -> Vector3 -> Ray
mkRay o d = Ray { rOrigin = o, rDirection = d }

mkSphere :: Float -> Vector3 -> Vector3 -> Shape
mkSphere r o c = Sphere { sOrigin = o, sRadius = r, sColor = c }

mkPlane :: Vector3 -> Vector3 -> Vector3 -> Shape
mkPlane o n c = Plane { pOrigin = o, pNormal = n, pColor = c }

data Camera = Camera { cPosition :: !Vector3, cDirection :: !Vector3 }

redHit :: Hit
redHit = Hit{ hColor = (1.0,0.0,0.0) }

checkerBoard :: Vector3 -> Hit
checkerBoard (x,y,z) = Hit{ hColor = s } where
    xT = round x * 255
    zT = round z * 255
    s  = if ( odd xT || odd zT ) && ( not ( odd xT && odd zT ) ) then (0.2,0.2,0.2) else (0.8,0.8,0.8)

blueHit :: Hit
blueHit = Hit{ hColor = (0.0,0.0,1.0) }

whiteHit :: Hit
whiteHit = Hit { hColor = (0.75,0.75,0.75) }

skyHit :: Ray -> Hit
skyHit Ray{ rOrigin = o, rDirection = d } = vectorHit (normalize d)

lightHit :: Ray -> Hit
lightHit Ray{ rOrigin = o, rDirection = d } = scalarHit $ 10.0 * ( saturateS ( normalize(d) <.> (0, 1.0, 0 ) ) ** 40.0 )

blackHit :: Hit
blackHit = Hit { hColor = (0, 0, 0) }

scalarHit :: Float -> Hit
scalarHit s = vectorHit (s,s,s)

colorHit :: Vector3 -> Hit
colorHit c = Hit { hColor = c }

vectorHit :: Vector3 -> Hit
vectorHit ( x, y, z ) = Hit { hColor = (x', y', z') } where
    x' = ( x * 0.5 + 0.5 )
    y' = ( y * 0.5 + 0.5 )
    z' = ( z * 0.5 + 0.5 )

colorToPixel :: Vector3 -> PixelRGB8 
colorToPixel (r,g,b) = PixelRGB8 x y z where
    x = round $ ( ( min 1.0 ( max r 0.0 ) ) ** (1.0/2.2) ) * 255
    y = round $ ( ( min 1.0 ( max g 0.0 ) ) ** (1.0/2.2) ) * 255
    z = round $ ( ( min 1.0 ( max b 0.0 ) ) ** (1.0/2.2) ) * 255 

rayCast :: Ray -> Shape -> ( Maybe Hit, Ray ) 

-- Ray-plane
rayCast r Plane{pOrigin = o, pNormal = n, pColor = col } = if ( lDotn /= 0 ) && ( d > 0 ) then ( Just (checkerBoard p ), r' ) else ( Nothing, r ) where
                                                lDotn = n <.> ( rDirection r )
                                                d     = ( ( o <-> ( rOrigin r ) ) <.> n ) / lDotn 
                                                p     = ( rOrigin r ) <+> ( ( rDirection r ) <*~ d )
                                                r'    = Ray { rOrigin = p, rDirection = reflect ( rDirection r ) n }
-- Ray-sphere
rayCast r s = if ( d > 0 ) && ( t > 0 ) then ( Just ( colorHit (sColor s) ), r' ) else ( Nothing, r ) where
                (dx,dy,dz) = ( rDirection r )
                (x0,y0,z0) = ( rOrigin r )
                (cx,cy,cz) = ( sOrigin s )  
                rad        = ( sRadius s )                                              
                a  = dx * dx + dy * dy + dz * dz                                                
                b  = 2 * dx * ( x0 - cx ) + 2 * dy * ( y0 - cy ) + 2 * dz * ( z0 - cz )
                c  = cx * cx + cy * cy + cz * cz + x0 * x0 + y0 * y0 + z0 * z0 - 2 * ( cx * x0 + cy * y0 + cz * z0 ) - ( rad * rad )
                d  = b * b - 4 * a * c
                t  = ( -b - sqrt( d ) ) / ( 2 * a )
                p  = ( x0 + t * dx, y0 + t * dy, z0 + t * dz ) 
                n  = normalize( p <-> ( sOrigin s ) )
                r' = Ray { rOrigin = p, rDirection = reflect ( rDirection r ) n }
                --r' = Ray { rOrigin = p, rDirection = refract ( rDirection r ) n (1.0/1.8) }

-- No hit
--rayCast r _ = ( Nothing, r )

accum :: Vector3 -> Vector3 -> Vector3
accum wi albedo = albedo <**> wi

rayTrace :: World -> Ray -> Int -> World -> Vector3
-- Ran out of 'things' to hit - fallback to our sky
rayTrace [] r _ _       = hColor $ lightHit r
-- Intersect again a 'thing'
rayTrace (x:xs) r i w   = case h' of 
                                Nothing -> rayTrace xs r i w
                                Just h  -> if ( i > 0 ) then accum sR (hColor h) else (hColor h)
                            where 
                                (h', r') = rayCast r x 
                                sR       = rayTrace w r' ( i - 1 ) w

rayDist :: Float
rayDist = 10.0

generateRay :: Float -> Float -> Camera -> Ray
generateRay x y c = mkRay (cPosition c) ( ( normalize (x, y, 2.0) ) <*~ rayDist )

defaultWorld :: World
defaultWorld = [ 
    mkSphere 0.25  ( 0.5, 0.25,  -0.25) (0.8, 0.1, 0.1),
    mkSphere 0.5   (-0.75, 0.5,   0.5) (0.1, 0.8, 0.1),
    mkSphere 0.125 ( 0.0,  0.125, -0.125) (0.5,0.5,0.8),
    mkPlane (0,0,0) (0,1,0) (0.01, 0.01, 0.015)
    ]

defaultCamera :: Camera 
defaultCamera = Camera { cPosition = (0,0.45,-1.5), cDirection = (0,0,1) }

width :: Int
width = 2048

height :: Int
height = 2048

numRays :: Int
numRays = 8

clipWidth :: Int -> Float
clipWidth x = ( (fromIntegral x) / (fromIntegral width) ) * 2.0 - 1.0

clipHeight :: Int -> Float
clipHeight y = -( ( (fromIntegral y) / (fromIntegral height) ) * 2.0 - 1.0 )

main :: IO ()
main = do 
    writePng "test.png" $ generateImage pixelRenderer width height
    putStrLn "Done"
        where pixelRenderer x y = colorToPixel $ r0 where -- <+> r1 <+> r2 <+> r3 ) </~ 4.0 where
                                    r0 = rayTrace defaultWorld ( generateRay (clipWidth x) (clipHeight y) defaultCamera ) numRays defaultWorld                                