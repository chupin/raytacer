# Readme

## Building the raytracer

Build the raytracer using the standard **cabal** build steps. It is recommended to 
use the **cabal sandbox**, as follows :

```
#!/bin/bash
cabal sandbox init # a no-op if the sandbox exists.
cabal install --only-dependencies
cabal build
```